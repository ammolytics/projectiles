[![pipeline status](https://gitlab.com/ammolytics/projectiles/badges/develop/pipeline.svg)](https://gitlab.com/ammolytics/projectiles/commits/develop)

# Open Source Projectile Dataset

A community-maintained dataset of ammunition projectile (bullet) information for use by developers, engineers, data scientists, and anyone who might find it useful.

# Status

This project is a **work-in-progress**.

**Contributions are very welcome, and encouraged!**

# Related projects

You may also be interested in the following projects.

* [cartridges](https://www.npmjs.com/package/cartridges): Open source dataset of all known ammunition cartridges
* [ammunition](https://www.npmjs.com/package/ammunition): Modern cartridges drawn to spec with D3


# Installation

## NodeJS

```
npm install projectiles
```

## Python

**Coming soon**


# Usage

## NodeJS

**Coming soon**

## Python

**Coming soon**


# Overview
**Work In Progress**


## Structure
**Work In Progress**


## Format

**Work In Progress**

The following template should be used when adding a projectile to the dataset. This template may evolve over time and backward-compatibility should be maintained to the best of our abilities. Language-specific packages based off of this dataset should use semantic versioning to ensure that format changes don't break userspace.


### Units of measure

Projectiles are often defined in both the Metric and Imperial units of measure. In order to provide maximum flexibility, **both units must be provided** in the specification file.

```
{
}
```


# References

* [Berger](http://www.bergerbullets.com/products/all-bullets/)
* [Sierra](https://www.sierrabullets.com/resources/ballistic-coefficients/)
* [Hornady](https://www.hornady.com/bullets/)
* [Lapua](http://www.lapua.com/en/reloading-components/bullets.html)